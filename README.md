# RPi wifi monitor

### service
Create a file `/etc/systemd/system/rpi-wifi-monitor.service` with content:
```
[Unit]
Description=rpi-wifi-monitor
After=network.target

[Service]
User=pi
WorkingDirectory=/home/pi/rpi-wifi-monitor
ExecStart=/home/pi/rpi-wifi-monitor/wifi-monitor.sh
User=pi
Group=pi
StandardOutput=journal
StandardError=journal

[Install]
WantedBy=default.target
```

Syntax check
```bash
sudo systemd-analyze verify /etc/systemd/system/rpi-wifi-monitor.service
```

### timer
Create a file `/etc/systemd/system/rpi-wifi-monitor.timer` with content:
```
[Unit]
Description=Check if wifi is working, try to restart it in case of problems, reboot the system on failure.
After=network.target

[Timer]
Unit=rpi-wifi-monitor.service
OnCalendar=*-*-* *:00:00

[Install]
WantedBy=default.target
```

Syntax check
```bash
sudo systemd-analyze verify /etc/systemd/system/rpi-wifi-monitor.timer
```

## Run program (every hour)
Enable service and timer with commands:
```bash
sudo systemctl enable rpi-wifi-monitor.service
sudo systemctl enable rpi-wifi-monitor.timer
```
