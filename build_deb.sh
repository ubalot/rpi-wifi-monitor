#!/usr/bin/env bash

set -e

package=$(grep Package < rpi-wifi-monitor/DEBIAN/control | cut -d ' ' -f 2)
version=$(grep Version < rpi-wifi-monitor/DEBIAN/control | cut -d ' ' -f 2)
architecture=$(grep Architecture < rpi-wifi-monitor/DEBIAN/control | cut -d ' ' -f 2)

mkdir -p rpi-wifi-monitor/usr/local/bin/
cp wifi-monitor.sh rpi-wifi-monitor/usr/local/bin/rpi-wifi-monitor.sh
dpkg-deb --build rpi-wifi-monitor
mv rpi-wifi-monitor.deb "${package}_${version}_${architecture}".deb
