#!/usr/bin/env bash

set -uo pipefail

ping_target="${ping_target:-192.168.1.1}"
wlan_if="${wlan_if:-wlan0}"

function if_connection_is_ok_then_exit() {
    if ping -i5 -c10 "$ping_target" > /dev/null
    then
        exit 0
    fi
}

function systemd_log() {
    systemd-cat echo "$@"
}

function log() {
    systemd_log "$@"
}

if_connection_is_ok_then_exit

if sudo iw dev "$wlan_if" get power_save | grep -c -i ": on" > /dev/null
then
    log "connection to $ping_target appears down; disabling power_save for $wlan_if"
    sudo iw dev "$wlan_if" set power_save off
    if_connection_is_ok_then_exit
fi

log "connection to $ping_target appears down; restarting $wlan_if and dhcpd!"
sudo ip link set "$wlan_if" down
sleep 10
sudo ip link set "$wlan_if" up
sudo systemctl restart dhcpcd
sleep 30
if_connection_is_ok_then_exit

log "connection to $ping_target remains down; restarting networking!!"
sudo systemctl restart networking
sleep 30
if_connection_is_ok_then_exit

log "connection to $ping_target remains down; will reboot shortly!!!"
sleep 15
if_connection_is_ok_then_exit
sudo reboot now
